Start menu for Linux/BSD. Auto-populates itself using the .desktop and .directory files found in
/usr/share, /usr/local/share, ~/.local/share and $XDG_DATA_DIRS.

# Dependencies

PyGtk2 and Gnome-menus.

On Debian/Ubuntu:

```
sudo apt-get install python-gtk2 gnome-menus
```

# Installation

```
wget 'https://gitlab.com/o9000/pmenu/repository/archive.tar.gz?ref=master' -O pmenu.tar.gz
tar xzf pmenu.tar.gz
cd pmenu
./install.sh
```

This copies the script to ~/bin, and the .desktop file to ~/.local/share/applications.
The script will work if $HOME/bin is in your $PATH variable.
To install instead to a system-wide location, add it as a parameter to install.sh:

```
./install.sh /usr/local
```

# Usage

See `pmenu.py -h` for all options. Here are the most common use cases:

## Show the builtin GTK menu

```
pmenu.py
```

## Display the menu at a fixed location instead of the mouse position

```
pmenu.py --x 10 --y 100
```

## Display settings, screen locking and logout buttons

```
pmenu.py --wm openbox
```

Currently, only openbox and xfce are supported. In openbox, locking the screen runs `slock`;
this program only displays a blank screen while typing your password.

It is not possible at the moment to set custom commands for these buttons;
but you can change them in the script code (search for `slock`).

## Generate an Openbox static menu

```
pmenu.py --mode openbox-static --wm openbox > ~/.config/openbox/menu.xml
```

## Generate an Openbox pipe menu

```
pmenu.py --mode openbox
```

This can be embedded into the Openbox menu by adding a `<menu>` tag to `~/.config/openbox/menu.xml`.
To show only the pmenu, you should have this in the config:

```
<openbox_menu>
  <menu id="root-menu" label="Openbox 3" execute="pmenu.py --mode openbox --wm openbox" />
</openbox_menu>
```

To show the pmenu as a submenu, embed it in another `<menu>`. Change the `id` attribute to `pmenu`.
In this example, we do not show the WM specific buttons in the pipe menu
(the `--wm openbox` parameter has been removed):

```
<openbox_menu>
  <menu id="root-menu" label="Openbox 3">
    <item label="Terminal">
      <action name="Execute">
        <execute>
          terminator
        </execute>
      </action>
    </item>
    <item label="Web Browser">
      <action name="Execute">
        <execute>
          x-www-browser
        </execute>
      </action>
    </item>

    <menu id="pmenu" label="Applications" execute="pmenu.py --mode openbox" />

    <separator />

    <item label="Settings">
      <action name="Execute">
         <command>
            obconf
         </command>
      </action>
    </item>
    <item label="Lock Screen">
      <action name="Execute">
         <command>
            slock
         </command>
      </action>
    </item>
    <item label="Log Out">
      <action name="Execute">
         <command>
            openbox --exit
         </command>
      </action>
    </item>
  </menu>
</openbox_menu>
```

After changing the openbox configuration, reload it with:

```
openbox --reconfigure
```

# Known issues

* Custom WM buttons are not supported

# Screenshots

[![pmenu.th.png](https://cdn.scrot.moe/images/2017/04/01/pmenu.th.png)](https://scrot.moe/image/1GhcX)

[![pmenu-pipea32ad.th.png](https://cdn.scrot.moe/images/2017/04/01/pmenu-pipea32ad.th.png)](https://scrot.moe/image/1L0hA)
